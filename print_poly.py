"""Pythonic code for extending gdb. DOES NOT INSTATIATE
"""
import gdb
from plotly.offline import plot
import plotly.graph_objs as go
import numpy as np

class PrintPolyCmd(gdb.Command):
  """Draws the content of (the outer contour of) boost::polygon `p`"""
  def __init__(self):
    """"Register the command name"""
    super(PrintPolyCmd,self).__init__("poly-pretty-print", gdb.COMMAND_USER)

  def _get_point(self,i):
    """Returns the corresponding point of the i-th boost::point from `p`
    """
    # see http://stackoverflow.com/a/253101/3637404
    thepoint='p.m_outer._M_impl._M_start[{0}]'.format(i)
    return map(float, \
      [ gdb.parse_and_eval('{0}.m_values[{1}]'.format(thepoint,0)), \
        gdb.parse_and_eval('{0}.m_values[{1}]'.format(thepoint,1))] )

  def invoke(self, arg, from_tty):
    """This is executed when I type poly-pretty-print in gdb"""
    #get data
    vsize = int(gdb.parse_and_eval('p.m_outer.size()'))
    pts=np.array([ self._get_point(i) for i in range(vsize)],dtype=np.dtype('float64'))
      
    data = [
      go.Scatter(
        x=pts[:,0],
        y=pts[:,1],
        mode='lines',
        name='somepoly'
        )
    ]
    #          ↓ "don't open the plot with browser"
    plot(data, auto_open=False, filename='~/gdb-plot.html')
